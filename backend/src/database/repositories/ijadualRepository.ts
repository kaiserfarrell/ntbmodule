import SequelizeRepository from '../../database/repositories/sequelizeRepository';
import AuditLogRepository from '../../database/repositories/auditLogRepository';
import lodash from 'lodash';
import SequelizeFilterUtils from '../../database/utils/sequelizeFilterUtils';
import Error404 from '../../errors/Error404';
import Sequelize from 'sequelize';

const Op = Sequelize.Op;

/**
 * Handles database operations for the Ijadual.
 * See https://sequelize.org/v5/index.html to learn how to customize it.
 */
class IjadualRepository {
  /**
   * Creates the Ijadual.
   *
   * @param {Object} data
   * @param {Object} [options]
   */
  static async create(data, options) {
    const currentUser = SequelizeRepository.getCurrentUser(
      options,
    );

    const tenant = SequelizeRepository.getCurrentTenant(
      options,
    );

    const transaction = SequelizeRepository.getTransaction(
      options,
    );

    const record = await options.database.ijadual.create(
      {
        ...lodash.pick(data, [
          'scheduleid',
          'activityCode',
          'collectionId',
          'contractorName',
          'date',
          'frequency',
          'location',
          'mainRoute',
          'parkId',
          'parkName',
          'pbt',
          'pbtId',
          'premiseType',
          'qtyPremis',
          'qtyTong',
          'route',
          'scheduledatacol',
          'schemeId',
          'schemeName',
          'ssmNumber',
          'streetId',
          'streetName',
          'timeEnd',
          'timeStart',
          'tongType',
          'vehicleNo',          
          'importHash',
        ]),
        tenantId: tenant.id,
        createdById: currentUser.id,
        updatedById: currentUser.id,
      },
      {
        transaction,
      },
    );


  

  
    await this._createAuditLog(
      AuditLogRepository.CREATE,
      record,
      data,
      options,
    );

    return this.findById(record.id, options);
  }

  /**
   * Updates the Ijadual.
   *
   * @param {Object} data
   * @param {Object} [options]
   */
  static async update(id, data, options) {
    const currentUser = SequelizeRepository.getCurrentUser(
      options,
    );

    const transaction = SequelizeRepository.getTransaction(
      options,
    );

    let record = await options.database.ijadual.findByPk(
      id,
      {
        transaction,
      },
    );

    const tenant = SequelizeRepository.getCurrentTenant(
      options,
    );

    if (
      !record ||
      String(record.tenantId) !== String(tenant.id)
    ) {
      throw new Error404();
    }

    record = await record.update(
      {
        ...lodash.pick(data, [
          'scheduleid',
          'activityCode',
          'collectionId',
          'contractorName',
          'date',
          'frequency',
          'location',
          'mainRoute',
          'parkId',
          'parkName',
          'pbt',
          'pbtId',
          'premiseType',
          'qtyPremis',
          'qtyTong',
          'route',
          'scheduledatacol',
          'schemeId',
          'schemeName',
          'ssmNumber',
          'streetId',
          'streetName',
          'timeEnd',
          'timeStart',
          'tongType',
          'vehicleNo',          
          'importHash',
        ]),
        updatedById: currentUser.id,
      },
      {
        transaction,
      },
    );





    await this._createAuditLog(
      AuditLogRepository.UPDATE,
      record,
      data,
      options,
    );

    return this.findById(record.id, options);
  }

  /**
   * Deletes the Ijadual.
   *
   * @param {string} id
   * @param {Object} [options]
   */
  static async destroy(id, options) {
    const transaction = SequelizeRepository.getTransaction(
      options,
    );

    let record = await options.database.ijadual.findByPk(
      id,
      {
        transaction,
      },
    );

    const tenant = SequelizeRepository.getCurrentTenant(
      options,
    );

    if (
      !record ||
      String(record.tenantId) !== String(tenant.id)
    ) {
      throw new Error404();
    }

    await record.destroy({
      transaction,
    });

    await this._createAuditLog(
      AuditLogRepository.DELETE,
      record,
      record,
      options,
    );
  }

  /**
   * Finds the Ijadual and its relations.
   *
   * @param {string} id
   * @param {Object} [options]
   */
  static async findById(id, options) {
    const transaction = SequelizeRepository.getTransaction(
      options,
    );

    const include = [

    ];

    const record = await options.database.ijadual.findByPk(
      id,
      {
        include,
        transaction,
      },
    );

    const tenant = SequelizeRepository.getCurrentTenant(
      options,
    );

    if (
      !record ||
      String(record.tenantId) !== String(tenant.id)
    ) {
      throw new Error404();
    }

    return this._fillWithRelationsAndFiles(record, options);
  }

  /**
   * Counts the number of Ijaduals based on the filter.
   *
   * @param {Object} filter
   * @param {Object} [options]
   */
  static async count(filter, options) {
    const transaction = SequelizeRepository.getTransaction(
      options,
    );

    const tenant = SequelizeRepository.getCurrentTenant(
      options,
    );

    return options.database.ijadual.count(
      {
        where: {
          ...filter,
          tenantId: tenant.id,
        },
      },
      {
        transaction,
      },
    );
  }

  /**
   * Finds the Ijaduals based on the query.
   * See https://sequelize.org/v5/manual/querying.html to learn how to
   * customize the query.
   *
   * @param {Object} query
   * @param {Object} query.filter
   * @param {number} query.limit
   * @param  {number} query.offset
   * @param  {string} query.orderBy
   * @param {Object} [options]
   *
   * @returns {Promise<Object>} response - Object containing the rows and the count.
   */
  static async findAndCountAll(
    { filter, limit = 0, offset = 0, orderBy = '' },
    options,
  ) {
    const tenant = SequelizeRepository.getCurrentTenant(
      options,
    );

    let whereAnd: Array<any> = [];
    let include = [
      
    ];

    whereAnd.push({
      tenantId: tenant.id,
    });

    if (filter) {
      if (filter.id) {
        whereAnd.push({
          ['id']: SequelizeFilterUtils.uuid(filter.id),
        });
      }

      if (filter.scheduleidRange) {
        const [start, end] = filter.scheduleidRange;

        if (start !== undefined && start !== null && start !== '') {
          whereAnd.push({
            scheduleid: {
              [Op.gte]: start,
            },
          });
        }

        if (end !== undefined && end !== null && end !== '') {
          whereAnd.push({
            scheduleid: {
              [Op.lte]: end,
            },
          });
        }
      }

      if (filter.activityCode) {
        whereAnd.push(
          SequelizeFilterUtils.ilike(
            'ijadual',
            'activityCode',
            filter.activityCode,
          ),
        );
      }

      if (filter.collectionIdRange) {
        const [start, end] = filter.collectionIdRange;

        if (start !== undefined && start !== null && start !== '') {
          whereAnd.push({
            collectionId: {
              [Op.gte]: start,
            },
          });
        }

        if (end !== undefined && end !== null && end !== '') {
          whereAnd.push({
            collectionId: {
              [Op.lte]: end,
            },
          });
        }
      }

      if (filter.contractorName) {
        whereAnd.push(
          SequelizeFilterUtils.ilike(
            'ijadual',
            'contractorName',
            filter.contractorName,
          ),
        );
      }

      if (filter.dateRange) {
        const [start, end] = filter.dateRange;

        if (start !== undefined && start !== null && start !== '') {
          whereAnd.push({
            date: {
              [Op.gte]: start,
            },
          });
        }

        if (end !== undefined && end !== null && end !== '') {
          whereAnd.push({
            date: {
              [Op.lte]: end,
            },
          });
        }
      }

      if (filter.frequency) {
        whereAnd.push(
          SequelizeFilterUtils.ilike(
            'ijadual',
            'frequency',
            filter.frequency,
          ),
        );
      }

      if (filter.location) {
        whereAnd.push(
          SequelizeFilterUtils.ilike(
            'ijadual',
            'location',
            filter.location,
          ),
        );
      }

      if (filter.mainRoute) {
        whereAnd.push(
          SequelizeFilterUtils.ilike(
            'ijadual',
            'mainRoute',
            filter.mainRoute,
          ),
        );
      }

      if (filter.parkIdRange) {
        const [start, end] = filter.parkIdRange;

        if (start !== undefined && start !== null && start !== '') {
          whereAnd.push({
            parkId: {
              [Op.gte]: start,
            },
          });
        }

        if (end !== undefined && end !== null && end !== '') {
          whereAnd.push({
            parkId: {
              [Op.lte]: end,
            },
          });
        }
      }

      if (filter.parkName) {
        whereAnd.push(
          SequelizeFilterUtils.ilike(
            'ijadual',
            'parkName',
            filter.parkName,
          ),
        );
      }

      if (filter.pbt) {
        whereAnd.push(
          SequelizeFilterUtils.ilike(
            'ijadual',
            'pbt',
            filter.pbt,
          ),
        );
      }

      if (filter.pbtIdRange) {
        const [start, end] = filter.pbtIdRange;

        if (start !== undefined && start !== null && start !== '') {
          whereAnd.push({
            pbtId: {
              [Op.gte]: start,
            },
          });
        }

        if (end !== undefined && end !== null && end !== '') {
          whereAnd.push({
            pbtId: {
              [Op.lte]: end,
            },
          });
        }
      }

      if (filter.premiseType) {
        whereAnd.push(
          SequelizeFilterUtils.ilike(
            'ijadual',
            'premiseType',
            filter.premiseType,
          ),
        );
      }

      if (filter.qtyPremisRange) {
        const [start, end] = filter.qtyPremisRange;

        if (start !== undefined && start !== null && start !== '') {
          whereAnd.push({
            qtyPremis: {
              [Op.gte]: start,
            },
          });
        }

        if (end !== undefined && end !== null && end !== '') {
          whereAnd.push({
            qtyPremis: {
              [Op.lte]: end,
            },
          });
        }
      }

      if (filter.qtyTongRange) {
        const [start, end] = filter.qtyTongRange;

        if (start !== undefined && start !== null && start !== '') {
          whereAnd.push({
            qtyTong: {
              [Op.gte]: start,
            },
          });
        }

        if (end !== undefined && end !== null && end !== '') {
          whereAnd.push({
            qtyTong: {
              [Op.lte]: end,
            },
          });
        }
      }

      if (filter.route) {
        whereAnd.push(
          SequelizeFilterUtils.ilike(
            'ijadual',
            'route',
            filter.route,
          ),
        );
      }

      if (filter.scheduledatacol) {
        whereAnd.push(
          SequelizeFilterUtils.ilike(
            'ijadual',
            'scheduledatacol',
            filter.scheduledatacol,
          ),
        );
      }

      if (filter.schemeIdRange) {
        const [start, end] = filter.schemeIdRange;

        if (start !== undefined && start !== null && start !== '') {
          whereAnd.push({
            schemeId: {
              [Op.gte]: start,
            },
          });
        }

        if (end !== undefined && end !== null && end !== '') {
          whereAnd.push({
            schemeId: {
              [Op.lte]: end,
            },
          });
        }
      }

      if (filter.schemeName) {
        whereAnd.push(
          SequelizeFilterUtils.ilike(
            'ijadual',
            'schemeName',
            filter.schemeName,
          ),
        );
      }

      if (filter.ssmNumberRange) {
        const [start, end] = filter.ssmNumberRange;

        if (start !== undefined && start !== null && start !== '') {
          whereAnd.push({
            ssmNumber: {
              [Op.gte]: start,
            },
          });
        }

        if (end !== undefined && end !== null && end !== '') {
          whereAnd.push({
            ssmNumber: {
              [Op.lte]: end,
            },
          });
        }
      }

      if (filter.streetIdRange) {
        const [start, end] = filter.streetIdRange;

        if (start !== undefined && start !== null && start !== '') {
          whereAnd.push({
            streetId: {
              [Op.gte]: start,
            },
          });
        }

        if (end !== undefined && end !== null && end !== '') {
          whereAnd.push({
            streetId: {
              [Op.lte]: end,
            },
          });
        }
      }

      if (filter.streetName) {
        whereAnd.push(
          SequelizeFilterUtils.ilike(
            'ijadual',
            'streetName',
            filter.streetName,
          ),
        );
      }

      if (filter.timeEnd) {
        whereAnd.push(
          SequelizeFilterUtils.ilike(
            'ijadual',
            'timeEnd',
            filter.timeEnd,
          ),
        );
      }

      if (filter.timeStart) {
        whereAnd.push(
          SequelizeFilterUtils.ilike(
            'ijadual',
            'timeStart',
            filter.timeStart,
          ),
        );
      }

      if (filter.tongType) {
        whereAnd.push(
          SequelizeFilterUtils.ilike(
            'ijadual',
            'tongType',
            filter.tongType,
          ),
        );
      }

      if (filter.vehicleNo) {
        whereAnd.push(
          SequelizeFilterUtils.ilike(
            'ijadual',
            'vehicleNo',
            filter.vehicleNo,
          ),
        );
      }

      if (filter.createdAtRange) {
        const [start, end] = filter.createdAtRange;

        if (
          start !== undefined &&
          start !== null &&
          start !== ''
        ) {
          whereAnd.push({
            ['createdAt']: {
              [Op.gte]: start,
            },
          });
        }

        if (
          end !== undefined &&
          end !== null &&
          end !== ''
        ) {
          whereAnd.push({
            ['createdAt']: {
              [Op.lte]: end,
            },
          });
        }
      }
    }

    const where = { [Op.and]: whereAnd };

    let {
      rows,
      count,
    } = await options.database.ijadual.findAndCountAll({
      where,
      include,
      limit: limit ? Number(limit) : undefined,
      offset: offset ? Number(offset) : undefined,
      order: orderBy
        ? [orderBy.split('_')]
        : [['createdAt', 'DESC']],
      transaction: SequelizeRepository.getTransaction(
        options,
      ),
    });

    rows = await this._fillWithRelationsAndFilesForRows(
      rows,
      options,
    );

    return { rows, count };
  }

  /**
   * Lists the Ijaduals to populate the autocomplete.
   * See https://sequelize.org/v5/manual/querying.html to learn how to
   * customize the query.
   *
   * @param {Object} query
   * @param {number} limit
   */
  static async findAllAutocomplete(query, limit, options) {
    const tenant = SequelizeRepository.getCurrentTenant(
      options,
    );

    let where: any = {
      tenantId: tenant.id,
    };

    if (query) {
      where = {
        ...where,
        [Op.or]: [
          { ['id']: SequelizeFilterUtils.uuid(query) },

        ],
      };
    }

    const records = await options.database.ijadual.findAll(
      {
        attributes: ['id', 'id'],
        where,
        limit: limit ? Number(limit) : undefined,
        orderBy: [['id', 'ASC']],
      },
    );

    return records.map((record) => ({
      id: record.id,
      label: record.id,
    }));
  }

  /**
   * Creates an audit log of the operation.
   *
   * @param {string} action - The action [create, update or delete].
   * @param {object} record - The sequelize record
   * @param {object} data - The new data passed on the request
   * @param {object} options
   */
  static async _createAuditLog(
    action,
    record,
    data,
    options,
  ) {
    let values = {};

    if (data) {
      values = {
        ...record.get({ plain: true }),

      };
    }

    await AuditLogRepository.log(
      {
        entityName: 'ijadual',
        entityId: record.id,
        action,
        values,
      },
      options,
    );
  }

  /**
   * Fills an array of Ijadual with relations and files.
   *
   * @param {Array} rows
   * @param {Object} [options]
   */
  static async _fillWithRelationsAndFilesForRows(
    rows,
    options,
  ) {
    if (!rows) {
      return rows;
    }

    return Promise.all(
      rows.map((record) =>
        this._fillWithRelationsAndFiles(record, options),
      ),
    );
  }

  /**
   * Fill the Ijadual with the relations and files.
   *
   * @param {Object} record
   * @param {Object} [options]
   */
  static async _fillWithRelationsAndFiles(record, options) {
    if (!record) {
      return record;
    }

    const output = record.get({ plain: true });

    const transaction = SequelizeRepository.getTransaction(
      options,
    );



    return output;
  }
}

export default IjadualRepository;
