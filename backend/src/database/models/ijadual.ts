import { DataTypes } from 'sequelize';
import moment from 'moment';

/**
 * Ijadual database model.
 * See https://sequelize.org/v5/manual/models-definition.html to learn how to customize it.
 */
export default function (sequelize) {
  const ijadual = sequelize.define(
    'ijadual',
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,
      },
      scheduleid: {
        type: DataTypes.INTEGER,
      },
      activityCode: {
        type: DataTypes.TEXT,
      },
      collectionId: {
        type: DataTypes.INTEGER,
      },
      contractorName: {
        type: DataTypes.TEXT,
      },
      date: {
        type: DataTypes.DATEONLY,
        get: function() {
          // @ts-ignore
          return this.getDataValue('date')
            ? moment
                // @ts-ignore
                .utc(this.getDataValue('date'))
                .format('YYYY-MM-DD')
            : null;
        },
      },
      frequency: {
        type: DataTypes.TEXT,
      },
      location: {
        type: DataTypes.TEXT,
      },
      mainRoute: {
        type: DataTypes.TEXT,
      },
      parkId: {
        type: DataTypes.INTEGER,
      },
      parkName: {
        type: DataTypes.TEXT,
      },
      pbt: {
        type: DataTypes.TEXT,
      },
      pbtId: {
        type: DataTypes.INTEGER,
      },
      premiseType: {
        type: DataTypes.TEXT,
      },
      qtyPremis: {
        type: DataTypes.INTEGER,
      },
      qtyTong: {
        type: DataTypes.INTEGER,
      },
      route: {
        type: DataTypes.TEXT,
      },
      scheduledatacol: {
        type: DataTypes.TEXT,
      },
      schemeId: {
        type: DataTypes.INTEGER,
      },
      schemeName: {
        type: DataTypes.TEXT,
      },
      ssmNumber: {
        type: DataTypes.INTEGER,
      },
      streetId: {
        type: DataTypes.INTEGER,
      },
      streetName: {
        type: DataTypes.TEXT,
      },
      timeEnd: {
        type: DataTypes.TEXT,
      },
      timeStart: {
        type: DataTypes.TEXT,
      },
      tongType: {
        type: DataTypes.TEXT,
      },
      vehicleNo: {
        type: DataTypes.TEXT,
      },
      importHash: {
        type: DataTypes.STRING(255),
        allowNull: true,        
      },
    },
    {
      indexes: [
        {
          unique: true,
          fields: ['importHash', 'tenantId'],
          where: {
            deletedAt: null,
          },
        },

      ],
      timestamps: true,
      paranoid: true,
    },
  );

  ijadual.associate = (models) => {



    
    models.ijadual.belongsTo(models.tenant, {
      as: 'tenant',
    });

    models.ijadual.belongsTo(models.user, {
      as: 'createdBy',
    });

    models.ijadual.belongsTo(models.user, {
      as: 'updatedBy',
    });
  };

  return ijadual;
}
