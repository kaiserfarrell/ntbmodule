import { DataTypes } from 'sequelize';

/**
 * Company database model.
 * See https://sequelize.org/v5/manual/models-definition.html to learn how to customize it.
 */
export default function (sequelize) {
  const company = sequelize.define(
    'company',
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,
      },
      companyName: {
        type: DataTypes.STRING(255),
        allowNull: false,
        validate: {
          len: [2, 255],
          notEmpty: true,
        }
      },
      sSMNumber: {
        type: DataTypes.TEXT,
        validate: {

        }
      },
      emailPIC: {
        type: DataTypes.TEXT,
        validate: {

        }
      },
      address: {
        type: DataTypes.TEXT,
        validate: {

        }
      },
      depoh: {
        type: DataTypes.TEXT,
      },
      scheme: {
        type: DataTypes.TEXT,
      },
      importHash: {
        type: DataTypes.STRING(255),
        allowNull: true,        
      },
    },
    {
      indexes: [
        {
          unique: true,
          fields: ['importHash', 'tenantId'],
          where: {
            deletedAt: null,
          },
        },

      ],
      timestamps: true,
      paranoid: true,
    },
  );

  company.associate = (models) => {


    models.company.hasMany(models.file, {
      as: 'photos',
      foreignKey: 'belongsToId',
      constraints: false,
      scope: {
        belongsTo: models.company.getTableName(),
        belongsToColumn: 'photos',
      },
    });
    
    models.company.belongsTo(models.tenant, {
      as: 'tenant',
    });

    models.company.belongsTo(models.user, {
      as: 'createdBy',
    });

    models.company.belongsTo(models.user, {
      as: 'updatedBy',
    });
  };

  return company;
}
