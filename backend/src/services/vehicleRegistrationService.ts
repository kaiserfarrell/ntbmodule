import VehicleRegistrationRepository from '../database/repositories/vehicleRegistrationRepository';
import Error400 from '../errors/Error400';
import SequelizeRepository from '../database/repositories/sequelizeRepository';

/**
 * Handles VehicleRegistration operations
 */
export default class VehicleRegistrationService {
  options;

  constructor(options) {
    this.options = options;
  }

  /**
   * Creates a VehicleRegistration.
   *
   * @param {*} data
   */
  async create(data) {
    const transaction = await SequelizeRepository.createTransaction(
      this.options.database,
    );

    try {
      const record = await VehicleRegistrationRepository.create(data, {
        ...this.options,
        transaction,
      });

      await SequelizeRepository.commitTransaction(
        transaction,
      );

      return record;
    } catch (error) {
      await SequelizeRepository.rollbackTransaction(
        transaction,
      );

      SequelizeRepository.handleUniqueFieldError(
        error,
        this.options.language,
        'vehicleRegistration',
      );

      throw error;
    }
  }

  /**
   * Updates a VehicleRegistration.
   *
   * @param {*} id
   * @param {*} data
   */
  async update(id, data) {
    const transaction = await SequelizeRepository.createTransaction(
      this.options.database,
    );

    try {
      const record = await VehicleRegistrationRepository.update(
        id,
        data,
        {
          ...this.options,
          transaction,
        },
      );

      await SequelizeRepository.commitTransaction(
        transaction,
      );

      return record;
    } catch (error) {
      await SequelizeRepository.rollbackTransaction(
        transaction,
      );

      SequelizeRepository.handleUniqueFieldError(
        error,
        this.options.language,
        'vehicleRegistration',
      );

      throw error;
    }
  }

  /**
   * Destroy all VehicleRegistrations with those ids.
   *
   * @param {*} ids
   */
  async destroyAll(ids) {
    const transaction = await SequelizeRepository.createTransaction(
      this.options.database,
    );

    try {
      for (const id of ids) {
        await VehicleRegistrationRepository.destroy(id, {
          ...this.options,
          transaction,
        });
      }

      await SequelizeRepository.commitTransaction(
        transaction,
      );
    } catch (error) {
      await SequelizeRepository.rollbackTransaction(
        transaction,
      );
      throw error;
    }
  }

  /**
   * Finds the VehicleRegistration by Id.
   *
   * @param {*} id
   */
  async findById(id) {
    return VehicleRegistrationRepository.findById(id, this.options);
  }

  /**
   * Finds VehicleRegistrations for Autocomplete.
   *
   * @param {*} search
   * @param {*} limit
   */
  async findAllAutocomplete(search, limit) {
    return VehicleRegistrationRepository.findAllAutocomplete(
      search,
      limit,
      this.options,
    );
  }

  /**
   * Finds VehicleRegistrations based on the query.
   *
   * @param {*} args
   */
  async findAndCountAll(args) {
    return VehicleRegistrationRepository.findAndCountAll(
      args,
      this.options,
    );
  }

  /**
   * Imports a list of VehicleRegistrations.
   *
   * @param {*} data
   * @param {*} importHash
   */
  async import(data, importHash) {
    if (!importHash) {
      throw new Error400(
        this.options.language,
        'importer.errors.importHashRequired',
      );
    }

    if (await this._isImportHashExistent(importHash)) {
      throw new Error400(
        this.options.language,
        'importer.errors.importHashExistent',
      );
    }

    const dataToCreate = {
      ...data,
      importHash,
    };

    return this.create(dataToCreate);
  }

  /**
   * Checks if the import hash already exists.
   * Every item imported has a unique hash.
   *
   * @param {*} importHash
   */
  async _isImportHashExistent(importHash) {
    const count = await VehicleRegistrationRepository.count(
      {
        importHash,
      },
      this.options,
    );

    return count > 0;
  }
}
