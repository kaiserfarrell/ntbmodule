import Permissions from '@/security/permissions';
import { PermissionChecker } from '@/modules/user/permission-checker';

export class IjadualPermissions {
  read: boolean;
  import: boolean;
  ijadualAutocomplete: boolean;
  create: boolean;
  edit: boolean;
  destroy: boolean;
  lockedForCurrentPlan: boolean;

  constructor(currentTenant, currentUser) {
    const permissionChecker = new PermissionChecker(
      currentTenant,
      currentUser,
    );

    this.read = permissionChecker.match(
      Permissions.values.ijadualRead,
    );
    this.import = permissionChecker.match(
      Permissions.values.ijadualImport,
    );
    this.ijadualAutocomplete = permissionChecker.match(
      Permissions.values.ijadualAutocomplete,
    );
    this.create = permissionChecker.match(
      Permissions.values.ijadualCreate,
    );
    this.edit = permissionChecker.match(
      Permissions.values.ijadualEdit,
    );
    this.destroy = permissionChecker.match(
      Permissions.values.ijadualDestroy,
    );
    this.lockedForCurrentPlan = permissionChecker.lockedForCurrentPlan(
      Permissions.values.ijadualRead,
    );
  }
}
