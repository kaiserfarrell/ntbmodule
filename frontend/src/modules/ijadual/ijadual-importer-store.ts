import importerStore from '@/shared/importer/importer-store';
import { IjadualService } from '@/modules/ijadual/ijadual-service';
import ijadualImporterFields from '@/modules/ijadual/ijadual-importer-fields';
import { i18n } from '@/i18n';

export default importerStore(
  IjadualService.import,
  ijadualImporterFields,
  i18n('entities.ijadual.importer.fileName'),
  i18n('entities.ijadual.importer.hint'),
);
