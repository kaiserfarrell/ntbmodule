import ijadualListStore from '@/modules/ijadual/ijadual-list-store';
import ijadualViewStore from '@/modules/ijadual/ijadual-view-store';
import ijadualImporterStore from '@/modules/ijadual/ijadual-importer-store';
import ijadualFormStore from '@/modules/ijadual/ijadual-form-store';
import ijadualDestroyStore from '@/modules/ijadual/ijadual-destroy-store';

export default {
  namespaced: true,

  modules: {
    destroy: ijadualDestroyStore,
    form: ijadualFormStore,
    list: ijadualListStore,
    view: ijadualViewStore,
    importer: ijadualImporterStore,
  },
};
