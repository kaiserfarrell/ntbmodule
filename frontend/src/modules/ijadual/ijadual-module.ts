import routes from '@/modules/ijadual/ijadual-routes';
import store from '@/modules/ijadual/ijadual-store';
import IjadualAutocompleteInput from '@/modules/ijadual/components/ijadual-autocomplete-input.vue';

export default {
  components: {
    'app-ijadual-autocomplete-input': IjadualAutocompleteInput,
  },
  routes,
  store,
};
