import Layout from '@/modules/layout/components/layout.vue';
import Permissions from '@/security/permissions';

const IjadualListPage = () =>
  import(
    '@/modules/ijadual/components/ijadual-list-page.vue'
  );
const IjadualFormPage = () =>
  import(
    '@/modules/ijadual/components/ijadual-form-page.vue'
  );
const IjadualViewPage = () =>
  import(
    '@/modules/ijadual/components/ijadual-view-page.vue'
  );
const IjadualImporterPage = () =>
  import(
    '@/modules/ijadual/components/ijadual-importer-page.vue'
  );

export default [
  {
    name: '',
    path: '',
    component: Layout,
    meta: { auth: true },
    children: [
      {
        name: 'ijadual',
        path: '/ijadual',
        component: IjadualListPage,
        meta: {
          auth: true,
          permission: Permissions.values.ijadualRead,
        },
      },
      {
        name: 'ijadualNew',
        path: '/ijadual/new',
        component: IjadualFormPage,
        meta: {
          auth: true,
          permission: Permissions.values.ijadualCreate,
        },
      },
      {
        name: 'ijadualImporter',
        path: '/ijadual/import',
        component: IjadualImporterPage,
        meta: {
          auth: true,
          permission: Permissions.values.ijadualImport,
        },
      },
      {
        name: 'ijadualEdit',
        path: '/ijadual/:id/edit',
        component: IjadualFormPage,
        meta: {
          auth: true,
          permission: Permissions.values.ijadualEdit,
        },
        props: true,
      },
      {
        name: 'ijadualView',
        path: '/ijadual/:id',
        component: IjadualViewPage,
        meta: {
          auth: true,
          permission: Permissions.values.ijadualRead,
        },
        props: true,
      },
    ],
  },
];
