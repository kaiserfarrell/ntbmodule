import { i18n } from '@/i18n';
import IdField from '@/shared/fields/id-field';
import { GenericModel } from '@/shared/model/generic-model';
import DateTimeRangeField from '@/shared/fields/date-time-range-field';
import DateTimeField from '@/shared/fields/date-time-field';
import IntegerField from '@/shared/fields/integer-field';
import IntegerRangeField from '@/shared/fields/integer-range-field';
import StringField from '@/shared/fields/string-field';
import DateField from '@/shared/fields/date-field';
import DateRangeField from '@/shared/fields/date-range-field';

function label(name) {
  return i18n(`entities.ijadual.fields.${name}`);
}

const fields = {
  id: new IdField('id', label('id')),
  scheduleid: new IntegerField('scheduleid', label('scheduleid'), {}),
  activityCode: new StringField('activityCode', label('activityCode'), {}),
  collectionId: new IntegerField('collectionId', label('collectionId'), {}),
  contractorName: new StringField('contractorName', label('contractorName'), {}),
  date: new DateField('date', label('date'), {}),
  frequency: new StringField('frequency', label('frequency'), {}),
  location: new StringField('location', label('location'), {}),
  mainRoute: new StringField('mainRoute', label('mainRoute'), {}),
  parkId: new IntegerField('parkId', label('parkId'), {}),
  parkName: new StringField('parkName', label('parkName'), {}),
  pbt: new StringField('pbt', label('pbt'), {}),
  pbtId: new IntegerField('pbtId', label('pbtId'), {}),
  premiseType: new StringField('premiseType', label('premiseType'), {}),
  qtyPremis: new IntegerField('qtyPremis', label('qtyPremis'), {}),
  qtyTong: new IntegerField('qtyTong', label('qtyTong'), {}),
  route: new StringField('route', label('route'), {}),
  scheduledatacol: new StringField('scheduledatacol', label('scheduledatacol'), {}),
  schemeId: new IntegerField('schemeId', label('schemeId'), {}),
  schemeName: new StringField('schemeName', label('schemeName'), {}),
  ssmNumber: new IntegerField('ssmNumber', label('ssmNumber'), {}),
  streetId: new IntegerField('streetId', label('streetId'), {}),
  streetName: new StringField('streetName', label('streetName'), {}),
  timeEnd: new StringField('timeEnd', label('timeEnd'), {}),
  timeStart: new StringField('timeStart', label('timeStart'), {}),
  tongType: new StringField('tongType', label('tongType'), {}),
  vehicleNo: new StringField('vehicleNo', label('vehicleNo'), {}),
  createdAt: new DateTimeField(
    'createdAt',
    label('createdAt'),
  ),
  updatedAt: new DateTimeField(
    'updatedAt',
    label('updatedAt'),
  ),
  createdAtRange: new DateTimeRangeField(
    'createdAtRange',
    label('createdAtRange'),
  ),
  scheduleidRange: new IntegerRangeField(
    'scheduleidRange',
    label('scheduleidRange'),
  ),
  collectionIdRange: new IntegerRangeField(
    'collectionIdRange',
    label('collectionIdRange'),
  ),
  dateRange: new DateRangeField(
    'dateRange',
    label('dateRange'),
  ),
  parkIdRange: new IntegerRangeField(
    'parkIdRange',
    label('parkIdRange'),
  ),
  pbtIdRange: new IntegerRangeField(
    'pbtIdRange',
    label('pbtIdRange'),
  ),
  qtyPremisRange: new IntegerRangeField(
    'qtyPremisRange',
    label('qtyPremisRange'),
  ),
  qtyTongRange: new IntegerRangeField(
    'qtyTongRange',
    label('qtyTongRange'),
  ),
  schemeIdRange: new IntegerRangeField(
    'schemeIdRange',
    label('schemeIdRange'),
  ),
  ssmNumberRange: new IntegerRangeField(
    'ssmNumberRange',
    label('ssmNumberRange'),
  ),
  streetIdRange: new IntegerRangeField(
    'streetIdRange',
    label('streetIdRange'),
  ),
};

export class IjadualModel extends GenericModel {
  static get fields() {
    return fields;
  }
}
