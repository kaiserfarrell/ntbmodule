import { IjadualModel } from '@/modules/ijadual/ijadual-model';

const { fields } = IjadualModel;

export default [
  fields.id,
  fields.scheduleid,
  fields.activityCode,
  fields.collectionId,
  fields.contractorName,
  fields.date,
  fields.frequency,
  fields.location,
  fields.mainRoute,
  fields.parkId,
  fields.parkName,
  fields.pbt,
  fields.pbtId,
  fields.premiseType,
  fields.qtyPremis,
  fields.qtyTong,
  fields.route,
  fields.scheduledatacol,
  fields.schemeId,
  fields.schemeName,
  fields.ssmNumber,
  fields.streetId,
  fields.streetName,
  fields.timeEnd,
  fields.timeStart,
  fields.tongType,
  fields.vehicleNo,
  fields.createdAt,
  fields.updatedAt
];
