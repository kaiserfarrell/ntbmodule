import Permissions from '@/security/permissions';
import { PermissionChecker } from '@/modules/user/permission-checker';

export class BackupVehiclePermissions {
  read: boolean;
  import: boolean;
  backupVehicleAutocomplete: boolean;
  create: boolean;
  edit: boolean;
  destroy: boolean;
  lockedForCurrentPlan: boolean;

  constructor(currentTenant, currentUser) {
    const permissionChecker = new PermissionChecker(
      currentTenant,
      currentUser,
    );

    this.read = permissionChecker.match(
      Permissions.values.backupVehicleRead,
    );
    this.import = permissionChecker.match(
      Permissions.values.backupVehicleImport,
    );
    this.backupVehicleAutocomplete = permissionChecker.match(
      Permissions.values.backupVehicleAutocomplete,
    );
    this.create = permissionChecker.match(
      Permissions.values.backupVehicleCreate,
    );
    this.edit = permissionChecker.match(
      Permissions.values.backupVehicleEdit,
    );
    this.destroy = permissionChecker.match(
      Permissions.values.backupVehicleDestroy,
    );
    this.lockedForCurrentPlan = permissionChecker.lockedForCurrentPlan(
      Permissions.values.backupVehicleRead,
    );
  }
}
