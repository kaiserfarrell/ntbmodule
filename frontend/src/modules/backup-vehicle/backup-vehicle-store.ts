import backupVehicleListStore from '@/modules/backup-vehicle/backup-vehicle-list-store';
import backupVehicleViewStore from '@/modules/backup-vehicle/backup-vehicle-view-store';
import backupVehicleImporterStore from '@/modules/backup-vehicle/backup-vehicle-importer-store';
import backupVehicleFormStore from '@/modules/backup-vehicle/backup-vehicle-form-store';
import backupVehicleDestroyStore from '@/modules/backup-vehicle/backup-vehicle-destroy-store';

export default {
  namespaced: true,

  modules: {
    destroy: backupVehicleDestroyStore,
    form: backupVehicleFormStore,
    list: backupVehicleListStore,
    view: backupVehicleViewStore,
    importer: backupVehicleImporterStore,
  },
};
