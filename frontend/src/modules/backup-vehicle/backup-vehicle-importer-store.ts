import importerStore from '@/shared/importer/importer-store';
import { BackupVehicleService } from '@/modules/backup-vehicle/backup-vehicle-service';
import backupVehicleImporterFields from '@/modules/backup-vehicle/backup-vehicle-importer-fields';
import { i18n } from '@/i18n';

export default importerStore(
  BackupVehicleService.import,
  backupVehicleImporterFields,
  i18n('entities.backupVehicle.importer.fileName'),
  i18n('entities.backupVehicle.importer.hint'),
);
