import routes from '@/modules/backup-vehicle/backup-vehicle-routes';
import store from '@/modules/backup-vehicle/backup-vehicle-store';
import BackupVehicleAutocompleteInput from '@/modules/backup-vehicle/components/backup-vehicle-autocomplete-input.vue';

export default {
  components: {
    'app-backup-vehicle-autocomplete-input': BackupVehicleAutocompleteInput,
  },
  routes,
  store,
};
