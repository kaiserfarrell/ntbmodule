import Layout from '@/modules/layout/components/layout.vue';
import Permissions from '@/security/permissions';

const BackupVehicleListPage = () =>
  import(
    '@/modules/backup-vehicle/components/backup-vehicle-list-page.vue'
  );
const BackupVehicleFormPage = () =>
  import(
    '@/modules/backup-vehicle/components/backup-vehicle-form-page.vue'
  );
const BackupVehicleViewPage = () =>
  import(
    '@/modules/backup-vehicle/components/backup-vehicle-view-page.vue'
  );
const BackupVehicleImporterPage = () =>
  import(
    '@/modules/backup-vehicle/components/backup-vehicle-importer-page.vue'
  );

export default [
  {
    name: '',
    path: '',
    component: Layout,
    meta: { auth: true },
    children: [
      {
        name: 'backupVehicle',
        path: '/backup-vehicle',
        component: BackupVehicleListPage,
        meta: {
          auth: true,
          permission: Permissions.values.backupVehicleRead,
        },
      },
      {
        name: 'backupVehicleNew',
        path: '/backup-vehicle/new',
        component: BackupVehicleFormPage,
        meta: {
          auth: true,
          permission: Permissions.values.backupVehicleCreate,
        },
      },
      {
        name: 'backupVehicleImporter',
        path: '/backup-vehicle/import',
        component: BackupVehicleImporterPage,
        meta: {
          auth: true,
          permission: Permissions.values.backupVehicleImport,
        },
      },
      {
        name: 'backupVehicleEdit',
        path: '/backup-vehicle/:id/edit',
        component: BackupVehicleFormPage,
        meta: {
          auth: true,
          permission: Permissions.values.backupVehicleEdit,
        },
        props: true,
      },
      {
        name: 'backupVehicleView',
        path: '/backup-vehicle/:id',
        component: BackupVehicleViewPage,
        meta: {
          auth: true,
          permission: Permissions.values.backupVehicleRead,
        },
        props: true,
      },
    ],
  },
];
