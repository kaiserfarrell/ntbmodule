import { BackupVehicleModel } from '@/modules/backup-vehicle/backup-vehicle-model';

const { fields } = BackupVehicleModel;

export default [
  fields.currentVehicleName,
  fields.newVehicle,
  fields.dateBackupStart,
  fields.dateBackupEnd,
  fields.route,
  fields.scheme,
];
