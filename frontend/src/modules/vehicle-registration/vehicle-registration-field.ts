import { VehicleRegistrationService } from '@/modules/vehicle-registration/vehicle-registration-service';
import RelationToOneField from '@/shared/fields/relation-to-one-field';
import RelationToManyField from '@/shared/fields/relation-to-many-field';
import Permissions from '@/security/permissions';

export class VehicleRegistrationField {
  static relationToOne(name, label, options) {
    return new RelationToOneField(
      name,
      label,
      '/vehicle-registration',
      Permissions.values.vehicleRegistrationRead,
      VehicleRegistrationService.listAutocomplete,
      (record) => {
        if (!record) {
          return null;
        }

        return {
          id: record.id,
          label: record.vehicleName,
        };
      },
      options,
    );
  }

  static relationToMany(name, label, options) {
    return new RelationToManyField(
      name,
      label,
      '/vehicle-registration',
      Permissions.values.vehicleRegistrationRead,
      VehicleRegistrationService.listAutocomplete,
      (record) => {
        if (!record) {
          return null;
        }

        return {
          id: record.id,
          label: record.vehicleName,
        };
      },
      options,
    );
  }
}
