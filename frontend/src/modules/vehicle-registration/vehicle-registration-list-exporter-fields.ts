import { VehicleRegistrationModel } from '@/modules/vehicle-registration/vehicle-registration-model';

const { fields } = VehicleRegistrationModel;

export default [
  fields.id,
  fields.vehicleName,
  fields.typeOfVehicle,
  fields.imei,
  fields.contractor,
  fields.route,
  fields.scheme,
  fields.createdAt,
  fields.updatedAt
];
