import Layout from '@/modules/layout/components/layout.vue';
import Permissions from '@/security/permissions';

const VehicleRegistrationListPage = () =>
  import(
    '@/modules/vehicle-registration/components/vehicle-registration-list-page.vue'
  );
const VehicleRegistrationFormPage = () =>
  import(
    '@/modules/vehicle-registration/components/vehicle-registration-form-page.vue'
  );
const VehicleRegistrationViewPage = () =>
  import(
    '@/modules/vehicle-registration/components/vehicle-registration-view-page.vue'
  );
const VehicleRegistrationImporterPage = () =>
  import(
    '@/modules/vehicle-registration/components/vehicle-registration-importer-page.vue'
  );

export default [
  {
    name: '',
    path: '',
    component: Layout,
    meta: { auth: true },
    children: [
      {
        name: 'vehicleRegistration',
        path: '/vehicle-registration',
        component: VehicleRegistrationListPage,
        meta: {
          auth: true,
          permission: Permissions.values.vehicleRegistrationRead,
        },
      },
      {
        name: 'vehicleRegistrationNew',
        path: '/vehicle-registration/new',
        component: VehicleRegistrationFormPage,
        meta: {
          auth: true,
          permission: Permissions.values.vehicleRegistrationCreate,
        },
      },
      {
        name: 'vehicleRegistrationImporter',
        path: '/vehicle-registration/import',
        component: VehicleRegistrationImporterPage,
        meta: {
          auth: true,
          permission: Permissions.values.vehicleRegistrationImport,
        },
      },
      {
        name: 'vehicleRegistrationEdit',
        path: '/vehicle-registration/:id/edit',
        component: VehicleRegistrationFormPage,
        meta: {
          auth: true,
          permission: Permissions.values.vehicleRegistrationEdit,
        },
        props: true,
      },
      {
        name: 'vehicleRegistrationView',
        path: '/vehicle-registration/:id',
        component: VehicleRegistrationViewPage,
        meta: {
          auth: true,
          permission: Permissions.values.vehicleRegistrationRead,
        },
        props: true,
      },
    ],
  },
];
