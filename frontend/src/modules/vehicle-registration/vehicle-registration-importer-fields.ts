import { VehicleRegistrationModel } from '@/modules/vehicle-registration/vehicle-registration-model';

const { fields } = VehicleRegistrationModel;

export default [
  fields.vehicleName,
  fields.typeOfVehicle,
  fields.imei,
  fields.contractor,
  fields.route,
  fields.scheme,
];
