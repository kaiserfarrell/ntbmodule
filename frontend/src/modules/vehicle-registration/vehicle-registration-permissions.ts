import Permissions from '@/security/permissions';
import { PermissionChecker } from '@/modules/user/permission-checker';

export class VehicleRegistrationPermissions {
  read: boolean;
  import: boolean;
  vehicleRegistrationAutocomplete: boolean;
  create: boolean;
  edit: boolean;
  destroy: boolean;
  lockedForCurrentPlan: boolean;

  constructor(currentTenant, currentUser) {
    const permissionChecker = new PermissionChecker(
      currentTenant,
      currentUser,
    );

    this.read = permissionChecker.match(
      Permissions.values.vehicleRegistrationRead,
    );
    this.import = permissionChecker.match(
      Permissions.values.vehicleRegistrationImport,
    );
    this.vehicleRegistrationAutocomplete = permissionChecker.match(
      Permissions.values.vehicleRegistrationAutocomplete,
    );
    this.create = permissionChecker.match(
      Permissions.values.vehicleRegistrationCreate,
    );
    this.edit = permissionChecker.match(
      Permissions.values.vehicleRegistrationEdit,
    );
    this.destroy = permissionChecker.match(
      Permissions.values.vehicleRegistrationDestroy,
    );
    this.lockedForCurrentPlan = permissionChecker.lockedForCurrentPlan(
      Permissions.values.vehicleRegistrationRead,
    );
  }
}
