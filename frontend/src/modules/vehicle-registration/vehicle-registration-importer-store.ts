import importerStore from '@/shared/importer/importer-store';
import { VehicleRegistrationService } from '@/modules/vehicle-registration/vehicle-registration-service';
import vehicleRegistrationImporterFields from '@/modules/vehicle-registration/vehicle-registration-importer-fields';
import { i18n } from '@/i18n';

export default importerStore(
  VehicleRegistrationService.import,
  vehicleRegistrationImporterFields,
  i18n('entities.vehicleRegistration.importer.fileName'),
  i18n('entities.vehicleRegistration.importer.hint'),
);
