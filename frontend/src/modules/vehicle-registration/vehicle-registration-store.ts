import vehicleRegistrationListStore from '@/modules/vehicle-registration/vehicle-registration-list-store';
import vehicleRegistrationViewStore from '@/modules/vehicle-registration/vehicle-registration-view-store';
import vehicleRegistrationImporterStore from '@/modules/vehicle-registration/vehicle-registration-importer-store';
import vehicleRegistrationFormStore from '@/modules/vehicle-registration/vehicle-registration-form-store';
import vehicleRegistrationDestroyStore from '@/modules/vehicle-registration/vehicle-registration-destroy-store';

export default {
  namespaced: true,

  modules: {
    destroy: vehicleRegistrationDestroyStore,
    form: vehicleRegistrationFormStore,
    list: vehicleRegistrationListStore,
    view: vehicleRegistrationViewStore,
    importer: vehicleRegistrationImporterStore,
  },
};
