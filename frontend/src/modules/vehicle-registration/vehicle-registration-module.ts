import routes from '@/modules/vehicle-registration/vehicle-registration-routes';
import store from '@/modules/vehicle-registration/vehicle-registration-store';
import VehicleRegistrationAutocompleteInput from '@/modules/vehicle-registration/components/vehicle-registration-autocomplete-input.vue';

export default {
  components: {
    'app-vehicle-registration-autocomplete-input': VehicleRegistrationAutocompleteInput,
  },
  routes,
  store,
};
