import { i18n } from '@/i18n';
import IdField from '@/shared/fields/id-field';
import { GenericModel } from '@/shared/model/generic-model';
import DateTimeRangeField from '@/shared/fields/date-time-range-field';
import DateTimeField from '@/shared/fields/date-time-field';
import StringField from '@/shared/fields/string-field';
import ImagesField from '@/shared/fields/images-field';
import Storage from '@/security/storage';

function label(name) {
  return i18n(`entities.company.fields.${name}`);
}

const fields = {
  id: new IdField('id', label('id')),
  companyName: new StringField('companyName', label('companyName'), {
    "required": true,
    "min": 2,
    "max": 255
  }),
  sSMNumber: new StringField('sSMNumber', label('sSMNumber'), {
    "max": 21845
  }),
  photos: new ImagesField('photos', label('photos'), Storage.values.companyPhotos, {
    "max": 3
  }),
  emailPIC: new StringField('emailPIC', label('emailPIC'), {
    "min": 6
  }),
  address: new StringField('address', label('address'), {
    "min": 6
  }),
  depoh: new StringField('depoh', label('depoh'), {}),
  scheme: new StringField('scheme', label('scheme'), {}),
  createdAt: new DateTimeField(
    'createdAt',
    label('createdAt'),
  ),
  updatedAt: new DateTimeField(
    'updatedAt',
    label('updatedAt'),
  ),
  createdAtRange: new DateTimeRangeField(
    'createdAtRange',
    label('createdAtRange'),
  ),

};

export class CompanyModel extends GenericModel {
  static get fields() {
    return fields;
  }
}
