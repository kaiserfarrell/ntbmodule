import Layout from '@/modules/layout/components/layout.vue';
import Permissions from '@/security/permissions';

const CompanyListPage = () =>
  import(
    '@/modules/company/components/company-list-page.vue'
  );
const CompanyFormPage = () =>
  import(
    '@/modules/company/components/company-form-page.vue'
  );
const CompanyViewPage = () =>
  import(
    '@/modules/company/components/company-view-page.vue'
  );
const CompanyImporterPage = () =>
  import(
    '@/modules/company/components/company-importer-page.vue'
  );

export default [
  {
    name: '',
    path: '',
    component: Layout,
    meta: { auth: true },
    children: [
      {
        name: 'company',
        path: '/company',
        component: CompanyListPage,
        meta: {
          auth: true,
          permission: Permissions.values.companyRead,
        },
      },
      {
        name: 'companyNew',
        path: '/company/new',
        component: CompanyFormPage,
        meta: {
          auth: true,
          permission: Permissions.values.companyCreate,
        },
      },
      {
        name: 'companyImporter',
        path: '/company/import',
        component: CompanyImporterPage,
        meta: {
          auth: true,
          permission: Permissions.values.companyImport,
        },
      },
      {
        name: 'companyEdit',
        path: '/company/:id/edit',
        component: CompanyFormPage,
        meta: {
          auth: true,
          permission: Permissions.values.companyEdit,
        },
        props: true,
      },
      {
        name: 'companyView',
        path: '/company/:id',
        component: CompanyViewPage,
        meta: {
          auth: true,
          permission: Permissions.values.companyRead,
        },
        props: true,
      },
    ],
  },
];
