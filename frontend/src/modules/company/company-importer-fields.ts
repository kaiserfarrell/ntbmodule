import { CompanyModel } from '@/modules/company/company-model';

const { fields } = CompanyModel;

export default [
  fields.companyName,
  fields.sSMNumber,
  fields.photos,
  fields.emailPIC,
  fields.address,
  fields.depoh,
  fields.scheme,
];
