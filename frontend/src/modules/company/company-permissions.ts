import Permissions from '@/security/permissions';
import { PermissionChecker } from '@/modules/user/permission-checker';

export class CompanyPermissions {
  read: boolean;
  import: boolean;
  companyAutocomplete: boolean;
  create: boolean;
  edit: boolean;
  destroy: boolean;
  lockedForCurrentPlan: boolean;

  constructor(currentTenant, currentUser) {
    const permissionChecker = new PermissionChecker(
      currentTenant,
      currentUser,
    );

    this.read = permissionChecker.match(
      Permissions.values.companyRead,
    );
    this.import = permissionChecker.match(
      Permissions.values.companyImport,
    );
    this.companyAutocomplete = permissionChecker.match(
      Permissions.values.companyAutocomplete,
    );
    this.create = permissionChecker.match(
      Permissions.values.companyCreate,
    );
    this.edit = permissionChecker.match(
      Permissions.values.companyEdit,
    );
    this.destroy = permissionChecker.match(
      Permissions.values.companyDestroy,
    );
    this.lockedForCurrentPlan = permissionChecker.lockedForCurrentPlan(
      Permissions.values.companyRead,
    );
  }
}
