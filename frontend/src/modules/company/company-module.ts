import routes from '@/modules/company/company-routes';
import store from '@/modules/company/company-store';
import CompanyAutocompleteInput from '@/modules/company/components/company-autocomplete-input.vue';

export default {
  components: {
    'app-company-autocomplete-input': CompanyAutocompleteInput,
  },
  routes,
  store,
};
