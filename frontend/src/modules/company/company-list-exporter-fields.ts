import { CompanyModel } from '@/modules/company/company-model';

const { fields } = CompanyModel;

export default [
  fields.id,
  fields.companyName,
  fields.sSMNumber,
  fields.photos,
  fields.emailPIC,
  fields.address,
  fields.depoh,
  fields.scheme,
  fields.createdAt,
  fields.updatedAt
];
