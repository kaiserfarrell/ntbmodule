import { CompanyService } from '@/modules/company/company-service';
import RelationToOneField from '@/shared/fields/relation-to-one-field';
import RelationToManyField from '@/shared/fields/relation-to-many-field';
import Permissions from '@/security/permissions';

export class CompanyField {
  static relationToOne(name, label, options) {
    return new RelationToOneField(
      name,
      label,
      '/company',
      Permissions.values.companyRead,
      CompanyService.listAutocomplete,
      (record) => {
        if (!record) {
          return null;
        }

        return {
          id: record.id,
          label: record.companyName,
        };
      },
      options,
    );
  }

  static relationToMany(name, label, options) {
    return new RelationToManyField(
      name,
      label,
      '/company',
      Permissions.values.companyRead,
      CompanyService.listAutocomplete,
      (record) => {
        if (!record) {
          return null;
        }

        return {
          id: record.id,
          label: record.companyName,
        };
      },
      options,
    );
  }
}
